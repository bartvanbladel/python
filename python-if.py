byte1 = 192
byte2 = 168
byte3 = 254
byte4 = 225
if byte1 > 255 or byte2 > 255 or byte3 > 255 or byte4 > 255:
    print("de vier bytes vormen geen geldig IPv4-adres")
if byte1 == 10 and byte2 <= 255 and byte3 <= 255 and byte4 <= 255:
    print("de vier bytes vormen een IPv4-adres in het bereik 10.0.0.0/8")
if byte1 != 10 and byte1 <= 255 and byte2 <= 255 and byte3 <= 255 and byte4 <= 255:
    print("de vier bytes vormen een IPv4-adres buiten het bereik 10.0.0.0/8")
